from selenium import webdriver
from bs4 import BeautifulSoup
from pprint import pprint
from time import sleep
import json

HS_EXTENTION = [
    '오리지널', '낙스라마스의 저주', '고블린 대 노움', '검은바위 산',
    '대 마상시합', '탐험가 연맹', '고대신의 속삭임', '한여름 밤의 카라잔',
    '비열한 거리의 가젯잔', '운고로를 향한 여정', '얼어붙은 왕좌의 기사들',
    '코볼트와 지하 미궁', '마녀숲', '폭심만만 프로젝트', '명예의 전당'
]

HS_JOBS = {
    'class-2' : '드루이드',
    'class-3' : '사냥꾼',
    'class-4' : '마법사',
    'class-5' : '성기사',
    'class-6' : '사제',
    'class-7' : '도적',
    'class-8' : '주술사',
    'class-9' : '흑마법사',
    'class-10': '전사',
    'class-12': '떡대/비취/카자쿠스'
}

db_list = []

# Chrome의 경우    : chromedriver의 위치를 지정해준다.
driver = webdriver.Chrome('./chromedriver')

# PhantomJS의 경우 : PhantomJS의 위치를 지정해준다.
# driver = webdriver.PhantomJS('./pantomjs')

for count in range(len(HS_EXTENTION)):
    if count == len(HS_EXTENTION) - 1:
        url = 'http://hs.inven.co.kr/dataninfo/card/#expansion=99'
    else:
        url = 'http://hs.inven.co.kr/dataninfo/card/#expansion={}'.format(count)
    driver.get(url)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    endPage = soup.find('div', {'id': 'hsDb'}).find('div', {'class': 'hsDbCommonList'})
    endPage = endPage.find('table').find('tfoot').find('tr', {'class': 'list-paging'}).find('td')
    endPage = endPage.find('span', {'class', 'paging'}).find_all('span')[-1].text

    for i in range(1, int(endPage) + 1):
        # 암묵적으로 웹 자원 로드를 위해 1초까지 기다려준다.
        driver.implicitly_wait(1)

        # 페이지 접속
        driver.get(url + ', page={}'.format(i))

        # 로딩이 완전히 끝날때까지 1초간 쉬기
        # sleep(1)

        # BeautifulSoup
        soup = BeautifulSoup(driver.page_source, 'html.parser')

        #hsDb > div.hsDbCommonList > table.hsDbCardTable.hsDbCardList > tbody
        notices = soup.find('div', {'id': 'hsDb'}).find('div', {'class': 'hsDbCommonList'})
        notices = notices.find('table').find('tbody').find_all('tr')
        for n in notices:
            if n == notices[-1]:
                break
            
            list = n.find_all('td')
            hs_name = list[1].find('b').text
            hs_type = list[2].text
            if hs_type == '영웅':
                continue
            hs_level = list[3].text
            if hs_level == '-':
                hs_level = '투기장 전용'
            if list[4].find('span') == None:
                hs_job = list[4].text
            else:
                hs_job = HS_JOBS[list[4].find('span')['class'][1]]
            hs_index = list[-1].text

            db_list.append({
                '확장팩': HS_EXTENTION[count],
                '이름': hs_name,
                '종류': hs_type,
                '등급': hs_level,
                '직업': hs_job,
                '효과': hs_index
            })

with open('hs_card_db.json', 'w', encoding='utf-8') as make_file:
    json.dump(db_list, make_file, ensure_ascii=False, indent='\t')